use std::{
    collections::HashSet,
    ffi::OsStr,
    fs,
    io::{Error, ErrorKind},
    path::Path,
};

use anyhow::{Context, Result};
use clap::Parser;

/// Cleans packages and build files from projects in workspace folder.
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Path to workspace folder
    #[arg(short, long)]
    workspace_path: String,

    /// Only display the folders to be removed without removing them.
    #[arg(short, long)]
    dry_run: bool,
}

fn main() -> Result<()> {
    let args = Args::parse();

    let workspace_path = args.workspace_path;
    let dry_run = args.dry_run;

    let folders_to_remove = find_folders_to_remove(&workspace_path)?;

    let mut total: u32 = 0;
    for folder in folders_to_remove {
        if dry_run {
            println!("would remove: {:}", folder);
        } else {
            println!("removing: {:}", folder);
            total += remove_folder(&folder)?;
        }
    }
    if !dry_run {
        println!("removed a total of {} files and folders", total);
    }
    Ok(())
}

fn find_folders_to_remove(workspace_path: &str) -> std::io::Result<HashSet<String>> {
    let mut folders_to_remove: HashSet<String> = HashSet::with_capacity(100);

    let mut queue: Vec<String> = Vec::with_capacity(100);

    queue.push(workspace_path.to_owned());

    let package_json: &OsStr = OsStr::new("package.json");
    let cargo_toml: &OsStr = OsStr::new("Cargo.toml");

    while let Some(folder) = queue.pop() {
        if folders_to_remove.contains(&folder) {
            continue;
        }

        for entry in fs::read_dir(folder)? {
            let entry = entry?;
            let path = entry.path();
            let path_string = path.to_string_lossy().to_string();
            if path.is_dir() {
                queue.push(path_string);
                continue;
            }
            if path.file_name() == Some(package_json) {
                let modules_folder = path
                    .parent()
                    .ok_or_else(|| Error::new(ErrorKind::Other, "missing parent folder"))?;
                let modules_folder = Path::join(modules_folder, "node_modules");
                if fs::metadata(&modules_folder).is_ok() {
                    folders_to_remove.insert(modules_folder.to_string_lossy().to_string());
                }
            } else if path.file_name() == Some(cargo_toml) {
                let targets_folder = path
                    .parent()
                    .ok_or_else(|| Error::new(ErrorKind::Other, "missing parent folder"))?;
                let targets_folder = Path::join(targets_folder, "target");
                if fs::metadata(&targets_folder).is_ok() {
                    folders_to_remove.insert(targets_folder.to_string_lossy().to_string());
                }
            }
        }
    }

    Ok(folders_to_remove)
}

fn remove_folder(folder: &str) -> Result<u32> {
    let mut count: u32 = 0;
    for entry in fs::read_dir(folder)? {
        let entry = entry?;
        let path = entry.path();
        let path_string = path.to_string_lossy().to_string();
        let lstat =
            fs::symlink_metadata(&path).with_context(|| format!("lstate path: {}", path_string))?;
        if path.is_dir() && lstat.is_dir() {
            count += remove_folder(&path_string)?;
        } else {
            fs::remove_file(&path).with_context(|| format!("remove file: {}", path_string))?;
            count += 1;
        }
    }
    fs::remove_dir(folder).with_context(|| format!("remove folder: {}", folder))?;
    Ok(count + 1)
}
