# clean-workspace

This is a utility to recursively remove packages and build files and folders from a workspace folder.

Currently supported project types:

- NodeJS / NPM (node_modules)
- Rust / Cargo (target)

The utility looks for a `package.json` file or a `Cargo.toml` file in a folder to determine if there is
a possibility of the `node_modules` folder for `package.json` files or `target` folder for `Cargo.toml`
files exists. If so the full path to the `node_modules` folder or `target` folder are queued and removed
at the end.

This is valuable if you have a workspace folder with many projects scattered throughout that you would like
to clean up these large folders without tracking them down yourself. Currently the tool only has a dry-run
flag to print the folders it has identified for removal.

Enhancements and bug fixes are welcome.
